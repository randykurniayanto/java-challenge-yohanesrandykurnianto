package ist.challenge.yohanes_randy_kurnianto.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ist.challenge.yohanes_randy_kurnianto.model.User;

@Repository
public interface UserRepo extends CrudRepository<User, Long> {
	public User findByUsernameAndPassword(String username, String password);

	public User findByUsername(String username);
}
