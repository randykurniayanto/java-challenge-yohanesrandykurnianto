package ist.challenge.yohanes_randy_kurnianto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ist.challenge.yohanes_randy_kurnianto.exception.IstChallengeException;
import ist.challenge.yohanes_randy_kurnianto.model.User;
import ist.challenge.yohanes_randy_kurnianto.service.UserService;

@RestController
@RequestMapping("/api/ist-challenge")
public class IstChallengeController {
	private UserService userService;

	@Autowired
	public IstChallengeController(UserService userService) {
		this.userService = userService;
	}

	@PostMapping("register")
	public ResponseEntity<String> register(@RequestBody User user) {
		try {
			userService.save(user);
			return buildResponse(null, HttpStatus.CREATED);
		} catch (IstChallengeException ex) {
			return buildResponse(ex.getMessage(), ex.getHttpStatus());
		}
	}

	@PostMapping("login")
	public ResponseEntity<String> login(@RequestBody User user) {
		try {
			userService.findByUsernameAndPassword(user);
			return buildResponse("Sukses Login", HttpStatus.OK);
		} catch (IstChallengeException ex) {
			return buildResponse(ex.getMessage(), ex.getHttpStatus());
		}

	}

	@GetMapping("users")
	public List<User> getUser() {
		return userService.get();
	}

	@PutMapping("users/edit/{id}")
	public ResponseEntity<String> editUser(@PathVariable(value = "id") Long id, @RequestBody User user) {
		try {
			userService.edit(id, user);
			return buildResponse(null, HttpStatus.CREATED);
		} catch (IstChallengeException ex) {
			return buildResponse(ex.getMessage(), ex.getHttpStatus());
		}
	}

	private ResponseEntity<String> buildResponse(String response, HttpStatus httpStatus) {
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<String>(response, headers, httpStatus);
	}
}
