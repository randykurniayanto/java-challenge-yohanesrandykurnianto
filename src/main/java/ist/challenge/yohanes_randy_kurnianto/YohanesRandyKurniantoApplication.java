package ist.challenge.yohanes_randy_kurnianto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YohanesRandyKurniantoApplication {

	public static void main(String[] args) {
		SpringApplication.run(YohanesRandyKurniantoApplication.class, args);
	}

}
