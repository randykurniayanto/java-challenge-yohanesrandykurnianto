package ist.challenge.yohanes_randy_kurnianto.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import ist.challenge.yohanes_randy_kurnianto.exception.IstChallengeException;
import ist.challenge.yohanes_randy_kurnianto.model.User;
import ist.challenge.yohanes_randy_kurnianto.repository.UserRepo;

@Service
public class UserService {
	private UserRepo repo;

	@Autowired
	public UserService(UserRepo repo) {
		this.repo = repo;
	}

	public List<User> get() {
		return (List<User>) repo.findAll();
	}

	public User findByUsernameAndPassword(User user) throws IstChallengeException {
		if (user.getUsername() == null || user.getPassword() == null || user.getUsername().isBlank()
				|| user.getPassword().isBlank()) {
			throw new IstChallengeException("Username dan / atau password kosong", HttpStatus.BAD_REQUEST);
		}

		User userByUsernameAndPassword = repo.findByUsernameAndPassword(user.getUsername(), user.getPassword());

		if (userByUsernameAndPassword == null) {
			throw new IstChallengeException(null, HttpStatus.UNAUTHORIZED);
		}
		return user;
	}

	public void save(User user) throws IstChallengeException {
		if (user.getUsername() == null || user.getPassword() == null || user.getUsername().isBlank()
				|| user.getPassword().isBlank()) {
			throw new IstChallengeException("Username dan / atau password kosong", HttpStatus.BAD_REQUEST);
		}

		User isExist = repo.findByUsername(user.getUsername());
		if (isExist != null) {
			throw new IstChallengeException("Username sudah terpakai", HttpStatus.CONFLICT);
		}
		repo.save(user);
	}

	public void edit(Long id, User user) throws IstChallengeException {
		if (user.getUsername() == null || user.getPassword() == null || user.getUsername().isBlank()
				|| user.getPassword().isBlank()) {
			throw new IstChallengeException("Username dan / atau password kosong", HttpStatus.BAD_REQUEST);
		}

		Optional<User> userById = repo.findById(id);
		User userByUsername = repo.findByUsername(user.getUsername());

		if (userById.isEmpty()) {
			throw new IstChallengeException(null, HttpStatus.NOT_FOUND);
		}

		if (userByUsername != null) {
			if (userById.get().getId() != userByUsername.getId()) {
				throw new IstChallengeException("Username sudah terpakai", HttpStatus.CONFLICT);
			}
		}

		if (user.getPassword().equals(userById.get().getPassword())) {
			throw new IstChallengeException("Password tidak boleh sama dengan password sebelumnya",
					HttpStatus.BAD_REQUEST);
		}

		user.setId(id);
		repo.save(user);
	}
}
