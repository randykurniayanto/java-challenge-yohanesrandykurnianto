package ist.challenge.yohanes_randy_kurnianto.exception;

import org.springframework.http.HttpStatus;

public class IstChallengeException extends Exception {
	private static final long serialVersionUID = 1L;

	HttpStatus httpStatus;

	public IstChallengeException() {
	}

	public IstChallengeException(String message) {
		super(message);
	}

	public IstChallengeException(String message, HttpStatus httpStatus) {
		super(message);
		this.httpStatus = httpStatus;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}

}
